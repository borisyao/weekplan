# weekPlan


## Development

To start application in the dev profile, simply run:

    ./mvnw


## Building app

### Packaging as jar

To build the final jar and optimize the weekPlan application for production, run:

    ./mvnw -Pprod clean verify


### Packaging as war

    ./mvnw -Pprod,war clean verify


### Code quality

Sonar is used to analyse code quality. You can start a local Sonar server (accessible on http://localhost:9001) with:

```
docker-compose -f src/main/docker/sonar.yml up -d
```

run a Sonar analysis:

```
./mvnw -Pprod clean verify sonar:sonar
```

## Using Docker 


To start a mysql database in a docker container, run:

    docker-compose -f src/main/docker/mysql.yml up -d

build a docker image of app by running:

    ./mvnw -Pprod verify jib:dockerBuild

Then run:

    docker-compose -f src/main/docker/app.yml up -d

