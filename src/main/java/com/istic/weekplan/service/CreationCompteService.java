
package com.istic.weekplan.service;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.istic.weekplan.domain.Personne;
import com.istic.weekplan.repository.PersonneRepository;
import com.istic.weekplan.security.SecurityUtils;
import com.istic.weekplan.service.dto.PersonneDTO;
import com.istic.weekplan.web.rest.errors.EmailAlreadyUsedException;
import com.istic.weekplan.web.rest.errors.InvalidPasswordException;

@Service
@Transactional
public class CreationCompteService {
	
    private final Logger log = LoggerFactory.getLogger(CreationCompteService.class);
    
    private final PasswordEncoder passwordEncoder;
    
    private final PersonneRepository personneRepository;

    public CreationCompteService(PersonneRepository personneRepository, PasswordEncoder passwordEncoder) {
        this.personneRepository = personneRepository;
        this.passwordEncoder = passwordEncoder;
    }
    
    public Personne inscrirePersonne(PersonneDTO personneDTO, String password) {
        personneRepository.findOneByEmailIgnoreCase(personneDTO.getEmail()).ifPresent(existingUser -> {
                throw new EmailAlreadyUsedException();
        });
        Personne newPersonne = new Personne();
        String encryptedPassword = passwordEncoder.encode(password);
        // new user gets initially a generated password
        newPersonne.setPassword(encryptedPassword);
        newPersonne.setFirstname(personneDTO.getFirstname());
        newPersonne.setName(personneDTO.getName());
        newPersonne.setEmail(personneDTO.getEmail().toLowerCase());
        newPersonne.setLogin(personneDTO.getLogin().toLowerCase());
        personneRepository.save(newPersonne);
        log.debug("Created Information for User: {}", newPersonne);
        return newPersonne;
    }
    
    public void changerPassword(String currentClearTextPassword, String newPassword) {
        SecurityUtils.getCurrentUserLogin()
            .flatMap(personneRepository::findOneByLogin)
            .ifPresent(personne -> {
                String currentEncryptedPassword = personne.getPassword();
                if (!passwordEncoder.matches(currentClearTextPassword, currentEncryptedPassword)) {
                    throw new InvalidPasswordException();
                }
                String encryptedPassword = passwordEncoder.encode(newPassword);
                personne.setPassword(encryptedPassword);
                log.debug("Mot de passe actualisé pour l'utilisateur: {}", personne);
            });
    }
    
    public void majInformations(String firstname, String lastName, String email) {        
        SecurityUtils.getCurrentUserLogin()
        .flatMap(personneRepository::findOneByLogin)
        .ifPresent(user -> {
            user.setFirstname(firstname);
            user.setName(lastName);
            user.setEmail(email.toLowerCase());
            log.debug("Changed Information for User: {}", user);
        });
    }
    
    @Transactional(readOnly = true)
    public Optional<Personne> getUser() {
        return SecurityUtils.getCurrentUserLogin().flatMap(personneRepository::findOneByLogin);
    }
    
	
}
