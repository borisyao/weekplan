package com.istic.weekplan.service.dto;

import java.util.ArrayList;
import java.util.List;

import com.istic.weekplan.domain.Activite;
import com.istic.weekplan.domain.ActivitePerso;
import com.istic.weekplan.domain.Lieu;

public class ConditionMeteoDTO {
	private long id;
	private int ventMax;
	private int ventMin;
	private int pluie;
	private int soleil;
	private int houleMin;
	private int houleMax;
	private List<Lieu> lieux = new ArrayList<Lieu>();
	private Activite activite;
	private ActivitePerso activitePerso;
	
	public ConditionMeteoDTO() {
		//leave empty
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getVentMax() {
		return ventMax;
	}

	public void setVentMax(int ventMax) {
		this.ventMax = ventMax;
	}

	public int getVentMin() {
		return ventMin;
	}

	public void setVentMin(int ventMin) {
		this.ventMin = ventMin;
	}

	public int getPluie() {
		return pluie;
	}

	public void setPluie(int pluie) {
		this.pluie = pluie;
	}

	public int getSoleil() {
		return soleil;
	}

	public void setSoleil(int soleil) {
		this.soleil = soleil;
	}

	public int getHouleMin() {
		return houleMin;
	}

	public void setHouleMin(int houleMin) {
		this.houleMin = houleMin;
	}

	public int getHouleMax() {
		return houleMax;
	}

	public void setHouleMax(int houleMax) {
		this.houleMax = houleMax;
	}

	public List<Lieu> getLieux() {
		return lieux;
	}

	public void setLieux(List<Lieu> lieux) {
		this.lieux = lieux;
	}

	public Activite getActivite() {
		return activite;
	}

	public void setActivite(Activite activite) {
		this.activite = activite;
	}

	public ActivitePerso getActivitePerso() {
		return activitePerso;
	}

	public void setActivitePerso(ActivitePerso activitePerso) {
		this.activitePerso = activitePerso;
	}
	
}
