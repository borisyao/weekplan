package com.istic.weekplan.service.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.istic.weekplan.domain.Activite;
import com.istic.weekplan.domain.ConditionMeteo;
import com.istic.weekplan.domain.Lieu;
import com.istic.weekplan.domain.Personne;

public class ActivitePersoDTO {
	private long id;
	private Personne personne;
	private Activite activite;
	private ConditionMeteo conditionMeteo;
	private List<Lieu> lieux = new ArrayList<Lieu>();
	
	public ActivitePersoDTO() {
		//leave empty
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Personne getPersonne() {
		return personne;
	}

	public void setPersonne(Personne personne) {
		this.personne = personne;
	}

	public Activite getActivite() {
		return activite;
	}

	public void setActivite(Activite activite) {
		this.activite = activite;
	}

	public ConditionMeteo getConditionMeteo() {
		return conditionMeteo;
	}

	public void setConditionMeteo(ConditionMeteo conditionMeteo) {
		this.conditionMeteo = conditionMeteo;
	}

	public List<Lieu> getLieux() {
		return lieux;
	}

	public void setLieux(List<Lieu> lieux) {
		this.lieux = lieux;
	}
	
}
