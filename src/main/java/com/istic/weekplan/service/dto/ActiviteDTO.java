package com.istic.weekplan.service.dto;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotBlank;

import com.istic.weekplan.domain.ActivitePerso;
import com.istic.weekplan.domain.ConditionMeteo;
import com.istic.weekplan.domain.Lieu;

public class ActiviteDTO {
	@NotBlank
	private String name;
	private List<Lieu> lieux = new ArrayList<Lieu>();
	private List<ActivitePerso> activitesPerso = new ArrayList<ActivitePerso>();
	private ConditionMeteo conditionMeteo;
	
	public ActiviteDTO() {
		//leave empty
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Lieu> getLieux() {
		return lieux;
	}

	public void setLieux(List<Lieu> lieux) {
		this.lieux = lieux;
	}

	public List<ActivitePerso> getActivitesPerso() {
		return activitesPerso;
	}

	public void setActivitesPerso(List<ActivitePerso> activitesPerso) {
		this.activitesPerso = activitesPerso;
	}

	public ConditionMeteo getConditionMeteo() {
		return conditionMeteo;
	}

	public void setConditionMeteo(ConditionMeteo conditionMeteo) {
		this.conditionMeteo = conditionMeteo;
	}
}
