package com.istic.weekplan.service.dto;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.istic.weekplan.config.Constants;
import com.istic.weekplan.domain.ActivitePerso;
import com.istic.weekplan.domain.Lieu;
import com.istic.weekplan.domain.Personne;

public class PersonneDTO {
	
    @NotBlank
    @Pattern(regexp = Constants.LOGIN_REGEX)
    @Size(min = 1, max = 50)
    private String login;
    
	@NotBlank
	@Email(message="Please provide a valid email address")
	@Pattern(regexp=".+@.+\\..+", message="Please provide a valid email address")
	private String email;
	private String name;
	private String firstname;
	private String password;
	private List<Lieu> lieux = new ArrayList<Lieu>();
	private List<ActivitePerso> activitesPerso = new ArrayList<ActivitePerso>();
	
    public static final int PASSWORD_MIN_LENGTH = 4;

    public static final int PASSWORD_MAX_LENGTH = 100;
	
	public PersonneDTO() {
		//leave empty
	}
	
	public PersonneDTO(Personne personne) {
		this.login = personne.getLogin();
        this.name = personne.getName();
        this.firstname = personne.getFirstname();
        this.email = personne.getEmail();
	}
	
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Lieu> getLieux() {
		return lieux;
	}

	public void setLieux(List<Lieu> lieux) {
		this.lieux = lieux;
	}

	public List<ActivitePerso> getActivitesPerso() {
		return activitesPerso;
	}

	public void setActivitesPerso(List<ActivitePerso> activitesPerso) {
		this.activitesPerso = activitesPerso;
	}
}
