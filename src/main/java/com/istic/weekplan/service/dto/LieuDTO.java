package com.istic.weekplan.service.dto;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotBlank;

import com.istic.weekplan.domain.Activite;
import com.istic.weekplan.domain.ActivitePerso;
import com.istic.weekplan.domain.ConditionMeteo;
import com.istic.weekplan.domain.Personne;

public class LieuDTO {

	@NotBlank
	private long id;
	private String ville;
	private String departement;
	private String region;
	private ConditionMeteo conditionMeteo;
	private List<Personne> personnes = new ArrayList<Personne>();
	private List<Activite> activites = new ArrayList<Activite>();
	private List<ActivitePerso> activitesPerso = new ArrayList<ActivitePerso>();
	
	public LieuDTO() {
		//leave empty
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getDepartement() {
		return departement;
	}

	public void setDepartement(String departement) {
		this.departement = departement;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public ConditionMeteo getConditionMeteo() {
		return conditionMeteo;
	}

	public void setConditionMeteo(ConditionMeteo conditionMeteo) {
		this.conditionMeteo = conditionMeteo;
	}

	public List<Personne> getPersonnes() {
		return personnes;
	}

	public void setPersonnes(List<Personne> personnes) {
		this.personnes = personnes;
	}

	public List<Activite> getActivites() {
		return activites;
	}

	public void setActivites(List<Activite> activites) {
		this.activites = activites;
	}

	public List<ActivitePerso> getActivitesPerso() {
		return activitesPerso;
	}

	public void setActivitesPerso(List<ActivitePerso> activitesPerso) {
		this.activitesPerso = activitesPerso;
	}
	
	
}
