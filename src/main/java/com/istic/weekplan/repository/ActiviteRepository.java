package com.istic.weekplan.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.istic.weekplan.domain.Activite;
import com.istic.weekplan.domain.ConditionMeteo;

@Repository
public interface ActiviteRepository extends CrudRepository<Activite, String>{
	
    //@Query("select a from Activite a where a.personne = :personne")
    //List<Activite> findActiviteByPersonne(String personne);
	
	@Query("select a from Activite a where a.name = :name")
    Optional<Activite> findActiviteByName(String name);
    
    @Query("select a from Activite a where lieu IN a.lieux")
    List<Activite> findActiviteByLieu(String lieu);
    
    @Query("select a from Activite a where a.conditionMeteo = :meteo")
    List<Activite> findActiviteByMeteo(ConditionMeteo meteo);
    
    
    
}
