package com.istic.weekplan.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.istic.weekplan.domain.ActivitePerso;
import com.istic.weekplan.domain.ConditionMeteo;

@Repository
public interface ActivitePersoRepository extends CrudRepository<ActivitePerso, String>{
	
    @Query("select ap from ActivitePerso ap where ap.personne = :personne")
    List<ActivitePerso> findActivitePersoByPersonne(String personne);
	
	//@Query("select ap from ActivitePerso where ap.name = :name")
    //Optional<ActivitePerso> findActivitePersoByName(String name);
    
    @Query("select ap from ActivitePerso ap where lieu IN ap.lieux")
    List<ActivitePerso> findActivitePersoByLieu(String lieu);
    
    @Query("select ap from ActivitePerso ap where ap.conditionMeteo = :meteo")
    List<ActivitePerso> findActivitePersoByMeteo(ConditionMeteo meteo);

	@Query("select ap from ActivitePerso ap")
	List<ActivitePerso> findAllActivitePerso();
}
