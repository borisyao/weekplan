package com.istic.weekplan.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.istic.weekplan.domain.ActivitePerso;
import com.istic.weekplan.domain.ConditionMeteo;
import com.istic.weekplan.domain.Lieu;

@Repository
public interface LieuRepository extends CrudRepository<Lieu, String>{
	
	@Query("select l from Lieu l where l.ville = :ville")
    Optional<Lieu> findLieuByVille(String ville);
	
	@Query("select l from Lieu l where l.departement = :departement")
    List<Lieu> findLieuByDepartement(String departement);
	
	@Query("select l from Lieu l where l.region = :region")
    List<Lieu> findLieuByRegion(String region);
	
    @Query("select l from Lieu l where personne IN l.personnes")
    List<Lieu> findLieuByPersonne(String personne);
    
    @Query("select l from Lieu l where activite IN l.activites")
    List<Lieu> findLieuByActivite(String activite);
    
    @Query("select l from Lieu l where activitePerso IN l.activitesPerso")
    List<Lieu> findLieuByActivitePerso(String activitePerso);
    
    @Query("select l from Lieu l where l.conditionMeteo = :meteo")
    List<Lieu> findLieuByMeteo(ConditionMeteo meteo);
    
	@Query("select l from Lieu l")
	List<Lieu> findAllLieu();
    
}
