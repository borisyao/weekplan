package com.istic.weekplan.repository;

import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.istic.weekplan.domain.Activite;
import com.istic.weekplan.domain.ActivitePerso;
import com.istic.weekplan.domain.ConditionMeteo;

@Repository
public interface ConditionMeteoRepository extends CrudRepository<ConditionMeteo, String>{
	
	Optional<ConditionMeteo> findOneById(long id);
	
	Optional<ConditionMeteo> findOneByActivite(Activite activite);
	
	Optional<ConditionMeteo> findOneByActivitePerso(ActivitePerso activite);
	
	Optional<ConditionMeteo> findOneByLieuxId(long id);
	
	//List<Lieu> findLieuxById(long id);
}
