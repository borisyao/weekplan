package com.istic.weekplan.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Lieu {
	private long id;
	private String ville;
	private String departement;
	private String region;
	private ConditionMeteo conditionMeteo;
	private List<Personne> personnes = new ArrayList<Personne>();
	private List<Activite> activites = new ArrayList<Activite>();
	private List<ActivitePerso> activitesPerso = new ArrayList<ActivitePerso>();
	
	public String getDepartement() {
		return departement;
	}

	public void setDepartement(String departement) {
		this.departement = departement;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	@ManyToOne
	public ConditionMeteo getConditionMeteo() {
		return conditionMeteo;
	}

	public void setConditionMeteo(ConditionMeteo conditionMeteo) {
		this.conditionMeteo = conditionMeteo;
	}

	@ManyToMany(mappedBy="lieux")
	public List<Personne> getPersonnes() {
		return personnes;
	}

	public void setPersonnes(List<Personne> personnes) {
		this.personnes = personnes;
	}

	@ManyToMany(mappedBy="lieux")
	public List<Activite> getActivites() {
		return activites;
	}

	public void setActivites(List<Activite> activites) {
		this.activites = activites;
	}

	@ManyToMany(mappedBy="lieux")
	public List<ActivitePerso> getActivitesPerso() {
		return activitesPerso;
	}

	public void setActivitesPerso(List<ActivitePerso> activitesPerso) {
		this.activitesPerso = activitesPerso;
	}

	@GeneratedValue
	@Id
	protected long getId() {
		return id;
	}

	protected void setId(long id) {
		this.id = id;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	

	public Lieu() {}
}
