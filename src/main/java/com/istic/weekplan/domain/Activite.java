package com.istic.weekplan.domain;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Activite {
	private String name;
	private List<Lieu> lieux = new ArrayList<Lieu>();
	private List<ActivitePerso> activitesPerso = new ArrayList<ActivitePerso>();
	private ConditionMeteo conditionMeteo;
	
	@ManyToMany
	public List<Lieu> getLieux() {
		return lieux;
	}

	public void setLieux(List<Lieu> lieux) {
		this.lieux = lieux;
	}

	@OneToMany(mappedBy="activite")
	public List<ActivitePerso> getActivitesPerso() {
		return activitesPerso;
	}

	public void setActivitesPerso(List<ActivitePerso> activitesPerso) {
		this.activitesPerso = activitesPerso;
	}
	
	@ManyToOne
	public ConditionMeteo getConditionMeteo() {
		return conditionMeteo;
	}

	public void setConditionMeteo(ConditionMeteo conditionMeteo) {
		this.conditionMeteo = conditionMeteo;
	}
	
	@Id
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Activite() {}
	
	
}
