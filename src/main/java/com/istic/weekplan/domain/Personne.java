package com.istic.weekplan.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.StringUtils;

import com.istic.weekplan.config.Constants;


@Entity
public class Personne {
	
    @NotNull
    @Pattern(regexp = Constants.LOGIN_REGEX)
    @Size(min = 1, max = 50)
    @Column(length = 50, unique = true, nullable = false)
    private String login;

	private String email;
	private String name;
	private String firstname;
	private String password;
	private List<Lieu> lieux = new ArrayList<Lieu>();
	private List<ActivitePerso> activitesPerso = new ArrayList<ActivitePerso>();
	
	
	@Id
    public String getLogin() {
        return login;
    }

    // Lowercase the login before saving it in database
    public void setLogin(String login) {
        this.login = StringUtils.lowerCase(login, Locale.ENGLISH);
    }
    
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@ManyToMany
	public List<Lieu> getLieux() {
		return lieux;
	}
	
	public void setLieux(List<Lieu> lieux) {
		this.lieux = lieux;
	}

	@OneToMany(mappedBy ="personne")
	public List<ActivitePerso> getActivitesPerso() {
		return activitesPerso;
	}

	public void setActivitesPerso(List<ActivitePerso> activitesPerso) {
		this.activitesPerso = activitesPerso;
	}


	public Personne() {}

	
}
