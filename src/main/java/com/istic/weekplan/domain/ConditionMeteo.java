package com.istic.weekplan.domain;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class ConditionMeteo {
	private long id;
	private int ventMax;
	private int ventMin;
	private int pluie;
	private int soleil;
	private int houleMin;
	private int houleMax;
	private List<Lieu> lieux = new ArrayList<Lieu>();
	private Activite activite;
	private ActivitePerso activitePerso;
	
	public int getVentMax() {
		return ventMax;
	}

	public void setVentMax(int ventMax) {
		this.ventMax = ventMax;
	}

	public int getVentMin() {
		return ventMin;
	}

	public void setVentMin(int ventMin) {
		this.ventMin = ventMin;
	}

	public int getPluie() {
		return pluie;
	}

	public void setPluie(int pluie) {
		this.pluie = pluie;
	}

	public int getSoleil() {
		return soleil;
	}

	public void setSoleil(int soleil) {
		this.soleil = soleil;
	}

	public int getHouleMin() {
		return houleMin;
	}

	public void setHouleMin(int houleMin) {
		this.houleMin = houleMin;
	}

	public int getHouleMax() {
		return houleMax;
	}

	public void setHouleMax(int houleMax) {
		this.houleMax = houleMax;
	}

	@OneToOne(mappedBy="conditionMeteo")
	public ActivitePerso getActivitePerso() {
		return activitePerso;
	}

	public void setActivitePerso(ActivitePerso activitePerso) {
		this.activitePerso = activitePerso;
	}

	@Id
	@GeneratedValue
	protected long getId() {
		return id;
	}

	protected void setId(long id) {
		this.id = id;
	}

	@OneToMany(mappedBy="conditionMeteo")
	public List<Lieu> getLieux() {
		return lieux;
	}

	public void setLieux(List<Lieu> lieux) {
		this.lieux = lieux;
	}

	@OneToOne(mappedBy="conditionMeteo")
	public Activite getActivite() {
		return activite;
	}
	
	public void setActivite(Activite activite) {
		this.activite = activite;
	}
	

	public ConditionMeteo() {}
}
