package com.istic.weekplan.web.rest;


import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.istic.weekplan.service.ActivitePersoService;
import com.istic.weekplan.service.LieuFavoriService;
import com.istic.weekplan.service.dto.ActivitePersoDTO;
import com.istic.weekplan.service.dto.LieuDTO;

/**
 * REST controller for managing the current user's account.
 */
@RestController
@RequestMapping("/api/profil")
public class ProfilResource {

    private final ActivitePersoService activitePersoService;
    
    private final LieuFavoriService lieuFavoriService;
    
    public ProfilResource(ActivitePersoService activitePersoService, LieuFavoriService lieuFavoriService) {
        this.activitePersoService = activitePersoService;
        this.lieuFavoriService = lieuFavoriService;
    }

    /**
     * {@code POST  /activitePerso} : create activite Perso.
     */
    @PostMapping("/activitePerso")
    @ResponseStatus(HttpStatus.CREATED)
    public void createActivitePerso(@Valid @RequestBody ActivitePersoDTO activitePersoDTO) {
    	activitePersoService.ajoutActivite(activitePersoDTO);
    }
    
    /**
     * {@code POST  /lieuFavoris} : create lieu favoris.
     */
    @PostMapping("/lieuFavoris")
    @ResponseStatus(HttpStatus.CREATED)
    public void createLieuFavoris(@Valid @RequestBody LieuDTO lieuDTO) {
    	lieuFavoriService.ajoutLieuFav(lieuDTO);
    }

}
