package com.istic.weekplan.web.rest;


import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.istic.weekplan.domain.Personne;
import com.istic.weekplan.repository.PersonneRepository;
import com.istic.weekplan.security.SecurityUtils;
import com.istic.weekplan.service.CreationCompteService;
import com.istic.weekplan.service.dto.PersonneDTO;
import com.istic.weekplan.web.rest.errors.EmailAlreadyUsedException;
import com.istic.weekplan.web.rest.errors.InvalidPasswordException;
import com.istic.weekplan.web.rest.errors.LoginAlreadyUsedException;

/**
 * REST controller for managing the current user's account.
 */
@RestController
@RequestMapping("/api")
public class CreationCompteResource {

    private static class CreationCompteResourceException extends RuntimeException {
        private CreationCompteResourceException(String message) {
            super(message);
        }
    }

    private final Logger log = LoggerFactory.getLogger(CreationCompteResource.class);

    private final PersonneRepository personneRepository;

    private final CreationCompteService creationCompteService;

    public CreationCompteResource(PersonneRepository personneRepository, CreationCompteService creationCompteService) {

        this.personneRepository = personneRepository;
        this.creationCompteService = creationCompteService;
    }

    /**
     * {@code POST  /register} : register the user.
     *
     * @param managedUserVM the managed user View Model.
     * @throws InvalidPasswordException {@code 400 (Bad Request)} if the password is incorrect.
     * @throws EmailAlreadyUsedException {@code 400 (Bad Request)} if the email is already used.
     * @throws LoginAlreadyUsedException {@code 400 (Bad Request)} if the login is already used.
     */
    @PostMapping("/inscrire")
    @ResponseStatus(HttpStatus.CREATED)
    public void registerAccount(@Valid @RequestBody PersonneDTO personneDTO) {
        if (!checkPasswordLength(personneDTO.getPassword())) {
            throw new InvalidPasswordException();
        }
        creationCompteService.inscrirePersonne(personneDTO, personneDTO.getPassword());
    }

    /**
     * {@code GET  /authenticate} : check if the user is authenticated, and return its login.
     *
     * @param request the HTTP request.
     * @return the login if the user is authenticated.
     */
    @GetMapping("/authentifie")
    public String isAuthenticated(HttpServletRequest request) {
        log.debug("REST request to check if the current user is authenticated");
        return request.getRemoteUser();
    }

    /**
     * {@code GET  /account} : get the current user.
     *
     * @return the current user.
     * @throws RuntimeException {@code 500 (Internal Server Error)} if the user couldn't be returned.
     */
    @GetMapping("/compte")
    public PersonneDTO getAccount() {
        return creationCompteService.getUser()
            .map(PersonneDTO::new)
            .orElseThrow(() -> new CreationCompteResourceException("User could not be found"));
    }

    /**
     * {@code POST  /account} : update the current user information.
     *
     * @param userDTO the current user information.
     * @throws EmailAlreadyUsedException {@code 400 (Bad Request)} if the email is already used.
     * @throws RuntimeException {@code 500 (Internal Server Error)} if the user login wasn't found.
     */
    @PostMapping("/compte")
    public void saveAccount(@Valid @RequestBody PersonneDTO personneDTO) {
        String userLogin = SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new CreationCompteResourceException("Current user login not found"));
        Optional<Personne> existingUser = personneRepository.findOneByEmailIgnoreCase(personneDTO.getEmail());
        if (existingUser.isPresent() && (!existingUser.get().getLogin().equalsIgnoreCase(userLogin))) {
            throw new EmailAlreadyUsedException();
        }
        Optional<Personne> user = personneRepository.findOneByLogin(userLogin);
        if (!user.isPresent()) {
            throw new CreationCompteResourceException("User could not be found");
        }
        creationCompteService.majInformations(personneDTO.getFirstname(), personneDTO.getName(), personneDTO.getEmail());
    }

    private static boolean checkPasswordLength(String password) {
        return !StringUtils.isEmpty(password) &&
            password.length() >= PersonneDTO.PASSWORD_MIN_LENGTH &&
            password.length() <= PersonneDTO.PASSWORD_MAX_LENGTH;
    }
}
